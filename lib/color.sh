#!/sh/hint

#{{{ color

colorize() {
    # prefer terminal safe colored and bold text when tput is supported
    if tput setaf 0 >/dev/null; then
        ALL_OFF="$(tput sgr0)"
        BOLD="$(tput bold)"
        BLUE="${BOLD}$(tput setaf 4)"
        GREEN="${BOLD}$(tput setaf 2)"
        RED="${BOLD}$(tput setaf 1)"
        YELLOW="${BOLD}$(tput setaf 3)"
        WHITE="${BOLD}$(tput setaf 7)"
    else
        ALL_OFF="\e[0m"
        BOLD="\e[1m"
        BLUE="${BOLD}\e[34m"
        GREEN="${BOLD}\e[32m"
        RED="${BOLD}\e[31m"
        YELLOW="${BOLD}\e[33m"
        WHITE="${BOLD}\e[37m"
    fi
    readonly ALL_OFF BOLD BLUE GREEN RED YELLOW WHITE
}

if [ -t 2 ] && [ "$TERM" != dumb ]; then
    colorize
else
    # shellcheck disable=2034
    ALL_OFF='' BOLD='' BLUE='' GREEN='' RED='' YELLOW=''
fi

msg(){
    mesg=$1; shift
    # shellcheck disable=2059
    printf "${WHITE} ${mesg}${ALL_OFF}\n" "$@"
}

error(){
    mesg=$1; shift
    # shellcheck disable=2059
    printf "${RED} ERROR:${ALL_OFF} ${WHITE}${mesg}${ALL_OFF}\n" "$@"
    exit 1
}

# }}}

#{{{ table

row_header(){
    mesg=$1; shift
    # shellcheck disable=2059
    printf "${BLUE} ${mesg} ${ALL_OFF}\n" "$@"
}

row_up(){
    mesg=$1; shift
    # shellcheck disable=2059
    printf "${GREEN} ${mesg}${ALL_OFF}\n" "$@"
}

row_down(){
    mesg=$1; shift
    # shellcheck disable=2059
    printf "${RED} ${mesg}${ALL_OFF}\n" "$@"
}

row_dep(){
    mesg=$1; shift
    # shellcheck disable=2059
    printf "${WHITE} |__${mesg}${ALL_OFF}\n" "$@"
}

#}}}
