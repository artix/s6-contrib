#!/sh/hint

#{{{ common

check_root() {
    [ "$(id -u)" = 0 ] && return
    if [ -f /usr/bin/sudo ]; then
        # shellcheck disable=SC2086
        exec sudo -- "$0" ${OPTS}
    else
        # shellcheck disable=2154
        exec su root -c "$0 ${OPTS}"
    fi
}

# }}}
